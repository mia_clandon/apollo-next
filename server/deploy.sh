#!/bin/bash

echo What should the version be?
read VERSION

docker build -t miaclandon07/apollo:$VERSION .
docker push miaclandon07/apollo:$VERSION
ssh root@64.227.13.208 "docker pull miaclandon07/apollo:$VERSION && docker tag miaclandon07/apollo:$VERSION dokku/api:$VERSION && dokku deploy api $VERSION"
