import React from 'react';
import {withUrqlClient} from "next-urql";
import {createUrqlClient} from "../../utils/createUrqlClient";
import {Layout} from "../../components/Layout";
import {Box, Heading} from "@chakra-ui/core";
import {useGetPostFromUrl} from "../../utils/useGetPostFromUrl";
import {EditDeletePostButtons} from "../../components/EditDeletePostButtons";
import {withApollo} from "../../utils/withApollo";


export const Post = ({}) => {
    const {data, error, loading} = useGetPostFromUrl();

    if (loading) {
        return (
            <Layout>
                <div>
                    loading...
                </div>
            </Layout>
        );
    }

    if (error) {
        return <div>{error.message}</div>
    }

    if (!data?.post) {
        return <Layout>
            <Box>Could don't find this post</Box>
        </Layout>
    }

    return (
        <Layout>
            <Heading
                mb={4}
            >
                {data.post.title}
            </Heading>
            <Box
                mb={4}
            >
                {data.post.text}
            </Box>
            <EditDeletePostButtons
                id={data.post.id}
                creatorId={data.post.creator.id}
            />
        </Layout>
    );
}

export default withApollo({ssr: true})(Post);


